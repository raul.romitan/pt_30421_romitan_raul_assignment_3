package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDAO {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO client (name,address)" + " VALUES (?,?)";
    private final static String findStatementString = "SELECT * FROM client where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM client where name = ?";
    private final static String deleteByNameStatementString = "DELETE FROM client where id = ?";
    private final static String findAllStatementString = "SELECT * FROM client";

    public static int insert(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, client.getName());
            insertStatement.setString(2, client.getAddress());
            insertStatement.executeUpdate();
            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static ArrayList<Client> getAllClients(){
        ArrayList<Client> clients = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement getAllStatement = null;
        ResultSet rs = null;
        try {
            getAllStatement = dbConnection.prepareStatement(findAllStatementString);
            rs = getAllStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String address = rs.getString("address");
                clients.add(new Client(id, name, address));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO: find all " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getAllStatement);
            ConnectionFactory.close(dbConnection);
        }
        /*
        for(Client c:clients){
            System.out.println(c);
        }
         */
        return clients;
    }

    public static Client getClientById(int idClient) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, idClient);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            toReturn = new Client(id, name, address);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO: getClientById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static Client getClientByName(String clientName) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, clientName);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            toReturn = new Client(id, name, address);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO: getClientByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }


    public static void deleteClientByName(String clientName) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        PreparedStatement deleteStatement;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, clientName);
            rs = findStatement.executeQuery();
            rs.next();
            int id = rs.getInt("id");
            deleteStatement = dbConnection.prepareStatement(deleteByNameStatementString);
            deleteStatement.setLong(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO: deleteClientByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
