package model;

public class Command {
    private final String command;
    private final String name;
    private String address;
    private String orderProduct;
    private int quantity;
    private float price;

    public Command(String command, String name, String orderProduct, int quantity) {
        this.command = command;
        this.name = name;
        this.orderProduct = orderProduct;
        this.quantity = quantity;
    }

    public Command(String command, String name, String address) {
        this.command = command;
        this.name = name;
        this.address = address;
    }

    public Command(String command, String name, int quantity, float price) {
        this.command = command;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Command(String command, String name) {
        this.command = command;
        this.name = name;
    }

    public String getCommand() {
        return command;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getOrderProduct() {
        return orderProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Command{" +
                "command='" + command + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", orderProduct='" + orderProduct + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
