package presentation;

import model.Command;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class FileParser {

    public ArrayList<Command> parse(String commandFile){
        Scanner in = null;
        try {
            in = new Scanner(new FileReader(commandFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<Command> commandList = new ArrayList<>();
        while(true){
            assert in != null;
            if (!in.hasNextLine()) break;
            String commandLine=in.nextLine();
            if(commandLine.contains(":")){
                String[] arrOfComm = commandLine.split(":");
                String commandName = arrOfComm[0];
                switch (commandName){
                    case "Insert client":
                    case "Delete client":
                        String[] clientData = arrOfComm[1].split(",");
                        commandList.add(new Command(commandName,clientData[0].substring(1),clientData[1].substring(1)));
                        break;
                    case "Insert product":
                        String[] productData = arrOfComm[1].split(",");
                        commandList.add(new Command(commandName,productData[0].substring(1),Integer.parseInt(productData[1].substring(1)),Float.parseFloat(productData[2].substring(1))));
                        break;
                    case "Delete Product":
                        commandList.add(new Command(commandName,arrOfComm[1].substring(1)));
                        break;
                    case "Order":
                        String[] orderData = arrOfComm[1].split(",");
                        commandList.add(new Command(commandName,orderData[0].substring(1),orderData[1].substring(1),Integer.parseInt(orderData[2].substring(1))));
                        break;
                    default:
                        System.out.println("De ce am ajuns aici?");
                        System.out.println(commandName);
                        break;
                }
            }else{
                String[] arrOfComm = commandLine.split(" ");
                commandList.add(new Command(arrOfComm[0],arrOfComm[1]));
            }
        }
        /*
        for(Command c:commandList){
            System.out.println(c);
        }
         */
        in.close();

        return commandList;
    }


}
