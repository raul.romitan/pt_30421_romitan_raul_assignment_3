package bll.validators;

import model.Client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientNameValidator implements Validator<Client> {
    public void validate(Client client) {
        Pattern p = Pattern.compile("([0-9])");
        Matcher m = p.matcher(client.getName());
        if(m.find()){
                throw new IllegalArgumentException("Client name is not valid!");
        }

    }
}
