package bll.validators;

import model.Product;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductNameValidator implements Validator<Product> {
    public void validate(Product product) {
        Pattern p = Pattern.compile("([0-9])");
        Matcher m = p.matcher(product.getName());
        if(m.find()){
            throw new IllegalArgumentException("Product name is not valid!");
        }

    }
}
