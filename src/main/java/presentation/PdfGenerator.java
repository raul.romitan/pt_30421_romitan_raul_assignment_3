package presentation;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.stream.Stream;

public class PdfGenerator {
    public void generateClientPdf(int pdfNb){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("client_report_nr_"+pdfNb+".pdf"));
            ArrayList<Client> clients= ClientDAO.getAllClients();
            document.open();
            PdfPTable table = new PdfPTable(3);
            addClientTableHeader(table);
            for(Client c:clients) {
                addClientRow(table, c);
            }

            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addClientTableHeader(PdfPTable table) {
        Stream.of("ID" ,"Name", "Address")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addClientRow(PdfPTable table, Client client) {
        table.addCell(String.valueOf(client.getId()));
        table.addCell(client.getName());
        table.addCell(client.getAddress());
    }

    public void generateProductPdf(int pdfNb){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("product_report_nr_"+pdfNb+".pdf"));
            ArrayList<Product> products= ProductDAO.getAllProducts();
            document.open();
            PdfPTable table = new PdfPTable(4);
            addProductTableHeader(table);
            for(Product p:products) {
                addProductRow(table, p);
            }

            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addProductTableHeader(PdfPTable table) {
        Stream.of("ID" ,"Name", "Quantity", "Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addProductRow(PdfPTable table, Product product) {
        table.addCell(String.valueOf(product.getId()));
        table.addCell(product.getName());
        table.addCell(String.valueOf(product.getQuantity()));
        table.addCell(String.valueOf(product.getPrice()));
    }

    public void generateOrderPdf(int pdfNb){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("orders_report_nr_"+pdfNb+".pdf"));
            ArrayList<Order> orders= OrderDAO.getAllOrders();
            document.open();
            PdfPTable table = new PdfPTable(4);
            addOrderTableHeader(table);
            for(Order o : orders) {
                addOrderRow(table, o);
            }

            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addOrderTableHeader(PdfPTable table) {
        Stream.of("ID" ,"Name", "Quantity", "Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addOrderRow(PdfPTable table, Order order) {
        table.addCell(String.valueOf(order.getId()));
        table.addCell(String.valueOf(ClientDAO.getClientById(order.getIdClient()).getName()));
        table.addCell(String.valueOf(ProductDAO.getProductById(order.getIdProduct()).getName()));
        table.addCell(String.valueOf(order.getQuantity()));

    }

    public void generateOrderCPdf(Client client,Product product,Order order,int orderID){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("order_id_"+orderID+".pdf"));
            document.open();
            PdfPTable table = new PdfPTable(5);
            addOrderCTableHeader(table);
            addOrderCRow(table,client,product,order);
            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addOrderCTableHeader(PdfPTable table) {
        Stream.of("ID" ,"Name", "Product", "Quantity", "TotalPrice")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addOrderCRow(PdfPTable table, Client client,Product product,Order order) {
        table.addCell(String.valueOf(order.getId()));
        table.addCell(String.valueOf(client.getName()));
        table.addCell(String.valueOf(product.getName()));
        table.addCell(String.valueOf(order.getQuantity()));
        table.addCell(String.valueOf(order.getQuantity() * product.getPrice()));
    }

    public void generateErrorOrderCPdf(Client client,Product product,Order order){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("order_err_"+client.getName()+"_"+product.getName()+".pdf"));
            document.open();
            PdfPTable table = new PdfPTable(5);
            addErrorOrderCTableHeader(table);
            addErrorOrderCRow(table,client,product,order);
            document.add(table);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addErrorOrderCTableHeader(PdfPTable table) {
        Stream.of("ID" ,"Name", "Product", "Quantity", "TotalPrice")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addErrorOrderCRow(PdfPTable table, Client client,Product product,Order order) {
        table.addCell(String.valueOf(order.getId()));
        table.addCell(String.valueOf(client.getName()));
        table.addCell(String.valueOf(product.getName()));
        table.addCell("NOT ENOUGH PRODUCTS IN STOCK");
        table.addCell("NOT ENOUGH PRODUCTS IN STOCK");
    }


}
