package bll.validators;

import model.Product;

public class ProductPriceValidator implements Validator<Product> {
    private static final int MIN_VALUE = 0;
    private static final int MAX_VALUE = 999999;
    public void validate(Product product) {
        if (product.getPrice() <= MIN_VALUE || product.getPrice() > MAX_VALUE) {
            throw new IllegalArgumentException("The product price limit is not respected!");
        }
    }
}
