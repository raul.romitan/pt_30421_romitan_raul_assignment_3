package bll;

import bll.validators.ClientNameValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientBLL {
    private static List<Validator<Client>> validators;
    public ClientBLL(){
        validators = new ArrayList<Validator<Client>>();
        validators.add(new ClientNameValidator());
    }

    /**
     *
     * @param client
     * @return
     */
    public static int insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return ClientDAO.insert(client);
    }
}
