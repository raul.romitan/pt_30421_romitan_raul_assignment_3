package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Command;
import model.Order;
import model.Product;

import java.util.ArrayList;

public class Controller {
    private int nbClientReport=1;
    private int nbProductReport=1;
    private int nbOrderReport=1;

    public Controller(String commandFileName) {
        FileParser fp = new FileParser();
        ArrayList<Command> commandList;
        commandList = fp.parse(commandFileName);
        startRunning(commandList);
    }
    public void startRunning(ArrayList<Command> commandList){
        PdfGenerator generator = new PdfGenerator();
        for(Command c:commandList){
            commandExecute(c,generator);
        }
    }

    public void commandExecute(Command command,PdfGenerator generator){
        ClientBLL cbll = new ClientBLL();
        ProductBLL pbll = new ProductBLL();
        switch (command.getCommand()){
            case "Insert client":
                cbll.insertClient(new Client(command.getName(),command.getAddress()));
                break;
            case "Delete client":
                ClientDAO.deleteClientByName(new Client(command.getName(),command.getAddress()).getName());
                break;
            case "Report":
                switch (command.getName()){
                    case "client":
                        generator.generateClientPdf(nbClientReport);
                        nbClientReport+=1;
                        break;
                    case "order":
                        generator.generateOrderPdf(nbOrderReport);
                        nbOrderReport+=1;
                        break;
                    case "product":
                        generator.generateProductPdf(nbProductReport);
                        nbProductReport+=1;
                        break;
                }
                break;
            case "Insert product":
                pbll.insertProduct(new Product(command.getName(),command.getQuantity(),command.getPrice()));
                break;
            case "Delete product":
                ArrayList<Order> orders;
                Product p = new Product(command.getName(),command.getQuantity(),command.getPrice());
                orders = OrderDAO.getAllOrders();
                for(Order o: orders){
                    if(ProductDAO.getProductByName(p.getName()).getId() == o.getIdProduct() ){
                        OrderDAO.deleteOrder(o);
                    }
                }
                ProductDAO.deleteProductByName(p.getName());
                break;
            case "Order":
                Client client = ClientDAO.getClientByName(command.getName());
                Product product = ProductDAO.getProductByName(command.getOrderProduct());
                Order order = new Order(client.getId(),product.getId() ,command.getQuantity());
                if(product.getQuantity() > order.getQuantity()) {
                    OrderBLL.insertOrder(order);
                    generator.generateOrderCPdf(client, product, order, OrderDAO.insert(order));
                    ProductDAO.updateItem(product,order);
                }else{
                    generator.generateErrorOrderCPdf(client,product,order);
                }
                break;
        }
    }
}
