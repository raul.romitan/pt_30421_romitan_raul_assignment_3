package dao;

import connection.ConnectionFactory;
import model.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class OrderDAO {
    protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO orders (idClient,idProduct,quantity)" + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM orders where id = ?";
    private final static String findAllStatementString = "SELECT * FROM orders";
    private final static String deleteByNameStatementString = "DELETE FROM orders where id = ?";

    /**
     *
     * @param order
     * @return
     */
    public static int insert(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);

            insertStatement.setInt(1, order.getIdClient());
            insertStatement.setInt(2, order.getIdProduct());
            insertStatement.setInt(3, order.getQuantity());

            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Order> getAllOrders(){
        ArrayList<Order> orders = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement getAllStatement = null;
        ResultSet rs = null;
        try {
            getAllStatement = dbConnection.prepareStatement(findAllStatementString);
            rs = getAllStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                int idClient = rs.getInt("idClient");
                int idProduct = rs.getInt("idProduct");
                int quantity = rs.getInt("quantity");

                orders.add(new Order(id, idClient, idProduct, quantity));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"OrderDAO: find all " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getAllStatement);
            ConnectionFactory.close(dbConnection);
        }
        /*
        for(Client c:clients){
            System.out.println(c);
        }
         */
        return orders;
    }

    /**
     *
     * @param order order to be deleted
     */
    public static void deleteOrder(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        PreparedStatement deleteStatement;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setInt(1, order.getId());
            rs = findStatement.executeQuery();
            rs.next();
            int id = rs.getInt("id");
            deleteStatement = dbConnection.prepareStatement(deleteByNameStatementString);
            deleteStatement.setLong(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"OrderDAO: deleteOrder " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
