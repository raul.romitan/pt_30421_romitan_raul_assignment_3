package bll;

import bll.validators.ProductNameValidator;
import bll.validators.ProductPriceValidator;
import bll.validators.ProductQuantityValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductBLL {
    private static List<Validator<Product>> validators;
    public ProductBLL(){
        validators = new ArrayList<>();
        validators.add(new ProductNameValidator());
        validators.add(new ProductPriceValidator());
        validators.add(new ProductQuantityValidator());
    }

    /**
     *
     * @param product
     */
    public static void insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        ProductDAO.insert(product);
    }
}
