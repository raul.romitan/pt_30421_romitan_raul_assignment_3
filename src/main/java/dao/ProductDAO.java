package dao;

import connection.ConnectionFactory;
import model.Order;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDAO {
    protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO product (name,quantity,price)" + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM product where id = ?";
    private final static String findAllStatementString = "SELECT * FROM product";
    private final static String findByNameStatementString = "SELECT * FROM product where name = ?";
    private final static String deleteByNameStatementString = "DELETE FROM product where id = ?";
    private final static String updateExistingProductStatementString = "UPDATE product SET quantity = quantity + ? where name = ?";


    public static int insert(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        PreparedStatement getAllStatement;
        PreparedStatement updateItem;
        ResultSet rs;
        try {
            getAllStatement = dbConnection.prepareStatement(findAllStatementString);
            rs = getAllStatement.executeQuery();
            boolean alreadyInTable=false;
            while(rs.next()) {
                String name = rs.getString("name");
                if(product.getName().equals(name)){
                    alreadyInTable=true;
                }
            }
            if(alreadyInTable){
                updateItem=dbConnection.prepareStatement(updateExistingProductStatementString);
                updateItem.setInt(1,product.getQuantity());
                updateItem.setString(2,product.getName());
                updateItem.executeUpdate();
            }else {
                insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, product.getName());
                insertStatement.setInt(2, product.getQuantity());
                insertStatement.setFloat(3, product.getPrice());

                insertStatement.executeUpdate();
                rs = insertStatement.getGeneratedKeys();
                if (rs.next()) {
                    insertedId = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void updateItem(Product product, Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement;
        PreparedStatement updateItem;
        ResultSet rs;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, product.getId());
            rs = findStatement.executeQuery();
            rs.next();
            updateItem=dbConnection.prepareStatement(updateExistingProductStatementString);
            updateItem.setInt(1,-order.getQuantity());
            updateItem.setString(2,product.getName());
            updateItem.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO: update " + e.getMessage());
        } finally {
            ConnectionFactory.close(dbConnection);
        }
    }

    public static ArrayList<Product> getAllProducts(){
        ArrayList<Product> products = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement getAllStatement = null;
        ResultSet rs = null;
        try {
            getAllStatement = dbConnection.prepareStatement(findAllStatementString);
            rs = getAllStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int quantity = rs.getInt("quantity");
                float price = rs.getFloat("price");
                products.add(new Product(id, name, quantity, price));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO: find all " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getAllStatement);
            ConnectionFactory.close(dbConnection);
        }
        /*
        for(Client c:clients){
            System.out.println(c);
        }
         */
        return products;
    }

    public static Product getProductById(int idProduct) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, idProduct);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            String name = rs.getString("name");
            int quantity = rs.getInt("quantity");
            float price = rs.getFloat("price");
            toReturn = new Product(id, name, quantity, price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO: getProductById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static Product getProductByName(String nameProduct) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, nameProduct);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            String name = rs.getString("name");
            int quantity = rs.getInt("quantity");
            float price = rs.getFloat("price");
            toReturn = new Product(id, name, quantity, price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO: getProductByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static void deleteProductByName(String productName) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        PreparedStatement deleteStatement;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, productName);
            rs = findStatement.executeQuery();
            rs.next();
            int id = rs.getInt("id");
            deleteStatement = dbConnection.prepareStatement(deleteByNameStatementString);
            deleteStatement.setLong(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO: deleteProductByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

}
