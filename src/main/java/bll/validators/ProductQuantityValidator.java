package bll.validators;

import model.Product;

public class ProductQuantityValidator implements Validator<Product> {
    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 999999;
    public void validate(Product product) {
        if (product.getPrice() < MIN_VALUE || product.getPrice() > MAX_VALUE) {
            throw new IllegalArgumentException("The product quantity limit is not respected!");
        }
    }
}
